// let buttons = document.querySelectorAll('.btn');
const  buttons = document.querySelectorAll(".btn-wrapper .btn");
 document.addEventListener('keypress', (event) => {
    buttonChange(event)
});

function buttonChange(event) {
    buttons.forEach(item => {
        item.style.background = '';
        if (item.innerText.toLowerCase() === event.key.toLowerCase()) {
            console.log(item);
            item.style.background = 'blue';
        }
    })
}

